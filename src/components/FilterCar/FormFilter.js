import React from "react";
import "./FormFilter.css";

export default function FormFilter() {
  return (
    <div>
      <section>
        <div className="container form-search row row-cols-xl-auto mx-auto p-1">
          <div className="col-xxl pt-3 mx-auto">
            <label for="form_driver" className="pb-2 form-label">
              Tipe Driver
            </label>
            <select name="" id="form_driver" className="form-select">
              <option value="" disabled selected>
                Pilih Tipe Driver
              </option>
              <option value="true">Dengan Sopir</option>
              <option value="false">Tanpa Sopir (Lepas Kunci)</option>
            </select>
          </div>
          <div className="col-xxl pt-3">
            <label for="form_date" className="pb-2 form-label">
              Tanggal
            </label>
            <input type="date" id="form_date" className="form-control" />
          </div>
          <div className="col-xxl pt-3 mx-auto">
            <label for="form_time" className="pb-2 form-label">
              Waktu Jemput/Ambil
            </label>
            <input
              type="time"
              className="form-control"
              id="form_time"
              placeholder="Pilih Waktu"
            />
          </div>
          <div className="col-xxl pt-3 mx-auto">
            <label for="form_pass" className="pb-2 form-label">
              Jumlah Penumpang (optional)
            </label>
            <input
              type="number"
              className="form-control"
              id="form_pass"
              placeholder="Jumlah Penumpang"
            />
          </div>
          <div
            className="p-3 flex-fill bd-highlight"
            // style="margin: auto; text-align: center"
          >
            <div className="d-grid gap-2 mx-auto">
              <button
                className="btn btn-sm btn-outline-success"
                type="button"
                id="load-btn"
              >
                Cari Mobil <i className="fa fa-solid fa-car"></i>
              </button>
              <button
                className="btn btn-sm btn-outline-primary"
                type="button"
                id="clear-btn"
              >
                Reset <i className="fa fa-solid fa-rotate-left"></i>
              </button>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
