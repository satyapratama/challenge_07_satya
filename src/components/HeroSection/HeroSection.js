import * as React from "react";
import "./HeroSection.css";
import imgCar from "./Mercedes Car EQC 300kW Edition - 900x598 1.svg";

export default function HeroSection() {
  return (
    <React.Fragment>
      <section id="hero-section">
        <div className="container d-flex">
          <div className="row row-cols-1 row-cols-md-1 row-cols-lg-2 g-md-1 g-lg-2">
            <div className="col my-5">
              <h1>Sewa dan Rental Mobil Terbaik di Kawasan (Lokasimu)</h1>
              <p>
                Selamat datang di Binar Car Rental. Kami menyediakan mobil
                kualitas terbaik dengan harga terjangkau. Selalu siap melayani
                kebutuhanmu untuk sewa mobil selama 24 jam.
              </p>
              <a href="cars">
                <button className="btn btn-success btn-msm" id="btnSewaMobil">
                  Mulai Sewa Mobil
                </button>
              </a>
            </div>
            <div className="col my-5">
              <img src={imgCar} className="img-fluid car" alt=".." />
            </div>
          </div>
        </div>
        <div className="car_bg bg-primary"></div>
      </section>
    </React.Fragment>
  );
}
