import * as React from "react";
import "./OurServices.css";
import imgService from "./img_service.svg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import "../../fontawesome";
export default function OurServices() {
  return (
    <div>
      <section id="our-services">
        <div className="container d-flex my-5">
          <div className="row row-cols-1 row-cols-md-1 row-cols-lg-2 g-md-1 g-lg-2">
            <div className="col my-5">
              <img src={imgService} className="img-fluid" alt="" />
            </div>
            <div className="col my-5">
              <h2>Best Car Rental for any kind of trip in (Lokasimu)!</h2>
              <p>
                Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga
                lebih murah dibandingkan yang lain, kondisi mobil baru, serta
                kualitas pelayanan terbaik untuk perjalanan wisata, bisnis,
                wedding, meeting, dll.
              </p>
              <div className="list-group">
                <li className="list-group-item os_list">
                  <i>
                    <FontAwesomeIcon
                      icon={["fas", "circle-check"]}
                      size="xl"
                      style={{ color: "Mediumslateblue" }}
                    />
                  </i>
                  Sewa Mobil Dengan Supir di Bali 12 Jam
                </li>
                <li className="list-group-item os_list">
                  <i>
                    <FontAwesomeIcon
                      icon={["fas", "circle-check"]}
                      size="xl"
                      style={{ color: "Mediumslateblue" }}
                    />
                  </i>
                  Sewa Mobil Lepas Kunci di Bali 24 Jam
                </li>
                <li className="list-group-item os_list">
                  <i>
                    <FontAwesomeIcon
                      icon={["fas", "circle-check"]}
                      size="xl"
                      style={{ color: "Mediumslateblue" }}
                    />
                  </i>
                  Sewa Mobil Jangka Panjang Bulanan
                </li>
                <li className="list-group-item os_list">
                  <i>
                    <FontAwesomeIcon
                      icon={["fas", "circle-check"]}
                      size="xl"
                      style={{ color: "Mediumslateblue" }}
                    />
                  </i>
                  Gratis Antar - Jemput Mobil di Bandara
                </li>
                <li className="list-group-item os_list">
                  <i>
                    <FontAwesomeIcon
                      icon={["fas", "circle-check"]}
                      size="xl"
                      style={{ color: "Mediumslateblue" }}
                    />
                  </i>
                  Layanan Airport Transfer / Drop In Out
                </li>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
