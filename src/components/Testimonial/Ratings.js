import * as React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Ratings() {
  return (
    <div className="rating p-2 text-warning">
      <i>
        <FontAwesomeIcon icon={["fas", "star"]} size="lg" />
      </i>
      <i>
        <FontAwesomeIcon icon={["fas", "star"]} size="lg" />
      </i>
      <i>
        <FontAwesomeIcon icon={["fas", "star"]} size="lg" />
      </i>
      <i>
        <FontAwesomeIcon icon={["fas", "star"]} size="lg" />
      </i>
      <i>
        <FontAwesomeIcon icon={["fas", "star"]} size="lg" />
      </i>
    </div>
    // <Stack spacing={1}>
    //   <Rating name="rating-read" defaultValue={5} readOnly />
    // </Stack>
  );
}
