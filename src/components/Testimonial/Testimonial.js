import React from "react";
import "./Testimonial.css";
import { useState } from "react";
import CardTestimonial from "./CardTestimonial";
// import * as image from "../../images";

export default function () {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };
  return (
    <div>
      <section id="testimonial">
        <div className="container py-5 justify-content-center align-items-center">
          <h2>Testimonial</h2>
          <p>Berbagai review positif dari para pelanggan kami</p>
          <div className="container justify-content-center">
            <div
              id="carouselExampleIndicators"
              className="carousel slide carousel-fade "
              activeIndex={index}
              onSelect={handleSelect}
              data-bs-ride="carousel"
            >
              <div className="carousel-indicators visually-hidden">
                <button
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide-to="0"
                  className="active bg-primary"
                  aria-label="Slide 1"
                ></button>
                <button
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide-to="1"
                  aria-label="Slide 2"
                  className="bg-primary"
                ></button>
                <button
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide-to="2"
                  aria-label="Slide 3"
                  className="bg-primary"
                ></button>
                <button
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide-to="3"
                  aria-label="Slide 4"
                  className="bg-primary"
                ></button>
                <button
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide-to="4"
                  aria-label="Slide 5"
                  className="bg-primary"
                ></button>
                <button
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide-to="5"
                  aria-label="Slide 6"
                  className="bg-primary"
                ></button>
              </div>
              <div className="carousel-inner">
                <CardTestimonial
                  carouselClass="carousel-item active"
                  img={require("../../assets/images/1.png")}
                  alt="1"
                  text="“Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit, sed do eiusmod lorem ipsum dolor sit amet,
                        consectetur adipiscing elit, sed do eiusmod lorem ipsum
                        dolor sit amet, consectetur adipiscing elit, sed do
                        eiusmod”"
                  name="Jae"
                  country="Surabaya"
                />
                <CardTestimonial
                  carouselClass="carousel-item"
                  img={require("../../assets/images/2.png")}
                  alt="2"
                  text="“Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit, sed do eiusmod lorem ipsum dolor sit amet,
                        consectetur adipiscing elit, sed do eiusmod lorem ipsum
                        dolor sit amet, consectetur adipiscing elit, sed do
                        eiusmod”"
                  name="Jae"
                  country="Surabaya"
                />
                <CardTestimonial
                  carouselClass="carousel-item "
                  img={require("../../assets/images/3.png")}
                  alt="2"
                  text="“Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit, sed do eiusmod lorem ipsum dolor sit amet,
                        consectetur adipiscing elit, sed do eiusmod lorem ipsum
                        dolor sit amet, consectetur adipiscing elit, sed do
                        eiusmod”"
                  name="Jae"
                  country="Surabaya"
                />
                <CardTestimonial
                  carouselClass="carousel-item"
                  img={require("../../assets/images/4.png")}
                  alt="2"
                  text="“Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit, sed do eiusmod lorem ipsum dolor sit amet,
                        consectetur adipiscing elit, sed do eiusmod lorem ipsum
                        dolor sit amet, consectetur adipiscing elit, sed do
                        eiusmod”"
                  name="Jae"
                  country="Surabaya"
                />
                <CardTestimonial
                  carouselClass="carousel-item"
                  img={require("../../assets/images/5.png")}
                  alt="2"
                  text="“Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit, sed do eiusmod lorem ipsum dolor sit amet,
                        consectetur adipiscing elit, sed do eiusmod lorem ipsum
                        dolor sit amet, consectetur adipiscing elit, sed do
                        eiusmod”"
                  name="Jae"
                  country="Surabaya"
                />
                <CardTestimonial
                  carouselClass="carousel-item"
                  img={require("../../assets/images/6.png")}
                  alt="2"
                  text="“Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit, sed do eiusmod lorem ipsum dolor sit amet,
                        consectetur adipiscing elit, sed do eiusmod lorem ipsum
                        dolor sit amet, consectetur adipiscing elit, sed do
                        eiusmod”"
                  name="Jae"
                  country="Surabaya"
                />
              </div>
            </div>
          </div>
          {/*Button Prev Next  */}
          <div className="d-flex justify-content-center align-items-center py-3">
            <button
              className="carousel-control-prev bg-danger rounded-circle btn_preNext"
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide="prev"
            >
              <span
                className="carousel-control-prev-icon"
                aria-hidden="true"
              ></span>
              <span className="visually-hidden">Previous</span>
            </button>
            <button
              className="carousel-control-next bg-success rounded-circle btn_preNext"
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide="next"
            >
              <span
                className="carousel-control-next-icon"
                aria-hidden="true"
              ></span>
              <span className="visually-hidden">Next</span>
            </button>
          </div>
          {/* End Button Prev Next */}
        </div>
      </section>
    </div>
  );
}
