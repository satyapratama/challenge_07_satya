import React from "react";
import "./Testimonial.css";
import Ratings from "./Ratings";

const CardTestimonial = (props) => {
  const { carouselClass, img, alt, text, name, country } = props;
  return (
    <div className={carouselClass}>
      <div className="card shadow-sm bg-secondary bg-opacity-25">
        <div className="d-flex d-xs-block d-sm-block d-md-flex p-5">
          <div className="card-img mx-3">
            <img src={img} alt={alt} className="card-img rounded-circle" />
          </div>
          <div className="card-text p-2">
            <div aria-hidden="true">
              <Ratings />
            </div>
            <p className="text-start">{text}</p>
            <h6 className="text-start fw-bold card-title">
              {name}, {country}
            </h6>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardTestimonial;
