import "../../App.css";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getListCars } from "../../actions/carsAction";
import { People, Gear, Calendar } from "react-bootstrap-icons";

function ListCars() {
  const { listCarsResult, listCarsLoading, listCarsError } = useSelector(
    (state) => state.carReducer
  );
  const dispatch = useDispatch();

  useEffect(() => {
    // get action list cars
    console.log("1. use effect component did mount");
    dispatch(getListCars());
  }, [dispatch]);

  return (
    <section>
      <div className="container list-cars row row-cols-1 row-cols-md-4 g-0 justify-content-md-center">
        {listCarsResult ? (
          listCarsResult.map((car) => {
            return (
              <div className="col m-3">
                <div className="card" style={{ width: "18rem" }}>
                  <img
                    src={car.image}
                    className="card-img-top img-fluid p-3 pt-4 mx-auto"
                    alt={car.plate}
                    style={{ width: "250px", maxHeight: "250px  " }}
                  />
                  <div className="card-body" style={{ fontSize: "14px" }}>
                    <p className="card-title" key={car.id}>
                      {car.manufacture} - {car.type}
                    </p>
                    <p
                      className="card-text text-start"
                      style={{ marginTop: "2px" }}
                    >
                      {car.description}
                    </p>
                    <div className="my-2">{car.rentPerDay} / Day</div>
                    <div className="my-2">
                      <People color="blue" size={16} className="me-2 my-1" />
                      {car.capacity}
                    </div>
                    <div className="my-2">
                      <Gear color="blue" size={16} className="me-2 my-1" />
                      {car.transmission}
                    </div>
                    <div className="my-2">
                      <Calendar color="blue" size={16} className="me-2 my-1" />
                      {car.availableAt}
                    </div>
                    <a
                      href="#"
                      className="btn btn-success text-white w-100 mt-4 fw-bold"
                      style={{ fontSize: "14px", marginBottom: "0" }}
                    >
                      Pilih Mobil
                    </a>
                  </div>
                </div>
              </div>
            );
          })
        ) : listCarsLoading ? (
          <p>Loading . . . </p>
        ) : (
          <p>{listCarsError ? listCarsError : "Data Kosong"}</p>
        )}
      </div>
    </section>
  );
}

export default ListCars;
