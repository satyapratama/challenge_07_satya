import ListCars from "./ListCars";
import "../App.css";
import Navbar from "../components/Navbar/Navbar";
import HeroSection from "../components/HeroSection/HeroSection";
import Footer from "../components/Footer/Footer";
import FormFilter from "../components/FilterCar/FormFilter";

function CariMobil() {
  return (
    <div className="App">
      <Navbar />
      <HeroSection />
      <FormFilter />
      <ListCars />
      <Footer />
    </div>
  );
}

export default CariMobil;
