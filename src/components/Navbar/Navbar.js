import * as React from "react";
import "./Navbar.css";
import { GoogleLogin } from '@react-oauth/google';
import { googleLogout } from '@react-oauth/google';
import { useNavigate } from "react-router-dom";
import { useEffect } from 'react';
// import { useGoogleLogin } from '@react-oauth/google';





const Navbar = () => {
  // const login = useGoogleLogin({onSuccess: tokenResponse => console.log(tokenResponse)});
  const navigate = useNavigate();

  useEffect(() => {
    var tokenResponse = localStorage.getItem('token-log');
    console.log(tokenResponse);
    if (tokenResponse == null) {
      document.getElementById("btn-login").hidden = false;
      document.getElementById("btn-logout").hidden = true;
    }
    else {
      document.getElementById("btn-login").hidden = true;
      document.getElementById("btn-logout").hidden = false;
    };
  }, []);

  function handleLogout(e) {
    e.preventDefault();
    localStorage.removeItem("token-log");
    localStorage.clear();
    document.getElementById("btn-login").hidden = false;
    document.getElementById("btn-logout").hidden = true;
    navigate("/");
    googleLogout();
    console.log("Logged Out")
  }

  return (
    <header>
      <nav className="navbar navbar-light navbar-expand-md sticky-top">
        <div className="container">
          <a className="navbar-brand">
            <div className="logo-brand"></div>
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div
            class="collapse navbar-collapse navbar-light"
            id="navbarNavAltMarkup"
          >
            <ul class="navbar-nav ms-auto">
              <li class="nav-item">
                <a class="nav-link px-3" href="#our-services">
                  Our Services
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link px-3" href="#why-us">
                  Why Us
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link px-3" href="#testimonial">
                  Testimonial
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link px-3" href="#faq">
                  FAQ
                </a>
              </li>
                <li class="nav-item" id="btn-login">
                <GoogleLogin
                  onSuccess={credentialResponse => {
                  console.log(credentialResponse);
                  const token = credentialResponse.credential;
                  localStorage.setItem('token-log', token);
                  console.log(token);
                  document.getElementById("btn-login").hidden = true;
                  document.getElementById("btn-logout").hidden = false;
                  }}
                  
                  onError={() => {
                  console.log('Login Failed');
                  }}
                />;
              </li>
              <li class="nav-item" id="btn-logout" >
                <button className="btn btn-danger" onClick={handleLogout}>
                  Logout
                </button>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
};
export default Navbar;
