import React from "react";
import "./Footer.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Footer() {
  return (
    <div>
      <footer class="footer text-lg-start">
        <div class="container py-3">
          <div class="container p-4">
            <div class="row mt-4">
              <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <ul class="list-unstyled">
                  <li>
                    <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                  </li>
                  <li>
                    <p>binarcarrental@gmail.com</p>
                  </li>
                  <li>
                    <p>081-233-334-808</p>
                  </li>
                </ul>
              </div>
              <div class="col-lg-3 col-md-6 mb-4 mb-md-0 ft_menu">
                <ul class="list-unstyled fw-bold">
                  <li>
                    <a href="#">Our Services</a>
                  </li>
                  <li>
                    <a href="#">Why Us</a>
                  </li>
                  <li>
                    <a href="#">Testimonial</a>
                  </li>
                  <li>
                    <a href="#">FAQ</a>
                  </li>
                </ul>
              </div>
              <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <p>Connect With Us</p>
                <ul class="list-unstyled ft_icon">
                  <li>
                    <a href="#!">
                      <i>
                        <FontAwesomeIcon
                          icon={["fab", "facebook"]}
                          size="xl"
                          style={{ color: "blue" }}
                        />
                      </i>
                    </a>
                    <a href="#!">
                      <i>
                        <FontAwesomeIcon
                          icon={["fab", "instagram"]}
                          size="xl"
                          style={{ color: "blue" }}
                        />
                      </i>
                    </a>
                    <a href="#!">
                      <i>
                        <FontAwesomeIcon
                          icon={["fab", "twitter"]}
                          size="xl"
                          style={{ color: "blue" }}
                        />
                      </i>
                    </a>
                    <a href="#!">
                      <i>
                        <FontAwesomeIcon
                          icon={["fas", "envelope"]}
                          size="xl"
                          style={{ color: "blue" }}
                        />
                      </i>
                    </a>
                    <a href="#!">
                      <i>
                        <FontAwesomeIcon
                          icon={["fab", "twitch"]}
                          size="xl"
                          style={{ color: "blue" }}
                        />
                      </i>
                    </a>
                  </li>
                </ul>
              </div>
              <div class="col brand">
                <p class="text-center">Copyright Binar 2022</p>
                <center>
                  <div class="logo"></div>
                </center>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}
