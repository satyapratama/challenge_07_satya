import React from "react";
import "./GettingStarted.css";

export default function GettingStarted() {
  return (
    <section id="getting-started">
      <div class="container py-5">
        <div class="card shadow bg-primary">
          <div class="d-block text-center p-5">
            <h1 class="fw-bold text-white">
              Sewa Mobil di (Lokasimu) Sekarang
            </h1>
            <p class="text-white py-5">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore
              fugit ut possimus nobis corrupti laborum totam accusamus
            </p>
            <a href="#" class="btn btn-success btn-msm" id="btnSewaMobil1">
              Mulai Sewa Mobil
            </a>
          </div>
        </div>
      </div>
    </section>
  );
}
