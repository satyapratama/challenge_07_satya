import React from "react";
import "./WhyUs.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const CardWU = (props) => {
  const { icons, title, description, style, bg } = props;
  return (
    <div className="col-md-4 col-xl-3">
      <div className="card order-card">
        <div className="card-block">
          <span className="wu_icon">
            <i aria-hidden="true">
              <FontAwesomeIcon icon={icons} size="xl" style={style} />
            </i>
          </span>
          <h6 className="fw-bold mt-3">{title}</h6>
          <p className="m-b-0">{description}</p>
        </div>
      </div>
    </div>
  );
};

export default CardWU;
