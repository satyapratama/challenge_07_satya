import * as React from "react";
import "./WhyUs.css";
import CardWU from "./CardWU";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function WhyUs() {
  return (
    <div>
      <section id="why-us">
        <div className="container">
          <h2>Why Us?</h2>
          <p>Mengapa harus pilih Binar Car Rental?</p>
          <div className="row">
            <CardWU
              icons={["fas", "thumbs-up"]}
              style={{ color: "orange" }}
              title="Mobil Lengkap"
              description="Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan
                  terawat"
            />
            <CardWU
              icons={["fas", "tags"]}
              style={{ color: "red" }}
              title="Harga Murah"
              description="Harga murah dan bersaing, bisa bandingkan harga kami dengan
                  rental mobil lain"
            />
            <CardWU
              icons={["fas", "clock"]}
              style={{ color: "Mediumblue" }}
              title="Layanan 24 Jam"
              description="Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga
                  tersedia di akhir minggu"
            />
            <CardWU
              icons={["fas", "award"]}
              style={{ color: "green" }}
              title="Sopir Profesional"
              description="Sopir yang profesional, berpengalaman, jujur, ramah dan selalu
                  tepat waktu"
            />
          </div>
        </div>
      </section>
    </div>
  );
}
