// import { hasGrantedAllScopesGoogle } from '@react-oauth/google';
import { Navigate } from "react-router-dom";

// TODO: Leave it to your imagination
function Protected({ children }) {
    var tokenResponse = localStorage.getItem('token-log');
    console.log(tokenResponse);
    if (tokenResponse == null) 
        return <Navigate to="/" />;
  return children;
}

export default Protected;
