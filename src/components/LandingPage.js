import "../App.css";
import HeroSection from "../components/HeroSection/HeroSection";
import Navbar from "../components/Navbar/Navbar";
import OurServices from "../components/OurServices/OurServices";
import WhyUs from "../components/WhyUs/WhyUs";
import Testimonial from "../components/Testimonial/Testimonial";
import GettingStarted from "./GettingStarted/GettingStarted";
import FAQ from "./FAQ/FAQ";
import Footer from "./Footer/Footer";

function LandingPage() {
  return (
    <div className="App">
      <Navbar />
      <HeroSection />
      <OurServices />
      <WhyUs />
      <Testimonial />
      <GettingStarted />
      <FAQ />
      <Footer />
    </div>
  );
}

export default LandingPage;
