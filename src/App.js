import logo from "./logo.svg";
import "./App.css";
import { ListCars } from "./components";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle";
import LandingPage from "./components/LandingPage";
import CariMobil from "./components/CariMobil";
import "./fontawesome";

function App() {
  return (
    // <div className="App" style={{ padding: "30px" }}>
    //   <h2> Aplikasi List Cars </h2>
    //   <hr />
    //   <ListCars />
    // </div>
    <div className="App">
      <LandingPage />
    </div>
  );
}

export default App;
