// import the library
import { library } from "@fortawesome/fontawesome-svg-core";

import { fab } from "@fortawesome/free-brands-svg-icons";
// import your icons
import {
  faCode,
  faHighlighter,
  faCheckCircle,
  faTags,
  faThumbsUp,
  faClock,
  faAward,
  faStar,
  faEnvelope,
} from "@fortawesome/free-solid-svg-icons";

library.add(
  faCode,
  faHighlighter,
  faCheckCircle,
  faTags,
  faThumbsUp,
  faClock,
  faAward,
  faStar,
  fab,
  faEnvelope
  // more icons go here
);
